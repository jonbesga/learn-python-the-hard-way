import web
from gothonweb import map

#web.config.debug = False

urls = (
    '/', 'index',
    '/game', 'gameengine',
    '/hello', 'hello',
    '/count', 'count',
    '/reset', 'reset',
)

app = web.application(urls, globals())

# little hack so that debug mode works with sessions
if web.config.get('_session') is None:
    store = web.session.DiskStore('sessions')
    session = web.session.Session(app, store, initializer={'room': None})
    web.config._session = session
else:
    session = web.config._session


#session_count = web.session.Session(app, store, initializer={'count': 0})

render = web.template.render('templates/', base="layout")

class index:
    def GET(self):
        # this is used to "setup" the session with starting values
        session.room = map.START
        web.seeother("/game")

class gameengine:
    def GET(self):
        if session.room:
            return render.show_room(room = session.room)
        else:
            # why is there here? do you need it?
            return render.you_died()
    def POST(self):
        form = web.input(action=None)

        # there is a bug here, can you fix it?
        if session.room and form.action:
            session.room = session.room.go(form.action)

        web.seeother("/game")

class count:
    def GET(self):
        session_count.count += 1
        return str(session_count.count)

class reset:
    def GET(self):
        session_count.kill()
        return ""

class hello(object):
    def GET(self):
        return render.hello_form()

    def POST(self):
        form = web.input(name="Nobody", greet="Hello")
        greeting = "%s, %s" % (form.greet, form.name)
        return render.index(greeting = greeting)

if __name__ == "__main__":
    app.run()