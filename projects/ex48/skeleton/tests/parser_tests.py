from nose.tools import *
from ex48.parser import *

def test_parse_subject():
    subj = parse_subject([('stop', 'The'), ('noun', 'Princess'), ('verb', 'run'), ('stop', 'to'), ('stop', 'the'), ('direction', 'north')])
    assert_equal(subj, ('noun', 'Princess'))

    subj = parse_subject([('verb', 'run'), ('direction', 'north')])
    assert_equal(subj, ('noun', 'player'))

    assert_raises(ParserError, parse_subject, [('stop', 'the')])

def test_parse_verb():
    verb = parse_verb([('verb', 'go'), ('stop', 'there')])
    assert_equal(verb, ('verb', 'go'))

def test_parse_object():
    obj = parse_object([('stop', 'to'), ('stop', 'the'), ('direction', 'north')])
    assert_equal(obj[1], 'north')

def test_parse_sentence():
    sentence = parse_sentence([('stop', 'The'), ('noun', 'Princess'), ('verb', 'run'), ('stop', 'to'), ('stop', 'the'), ('direction', 'north')])
    assert_equal(sentence.subject, 'Princess')
    assert_equal(sentence.verb, 'run')
    assert_equal(sentence.object, 'north')