sentence = [('verb', 'go'), ('direction', 'north'), ('direction', 'south'), ('direction', 'east'), ('verb', 'kill'), ('verb', 'eat'), ('stop', 'the'), ('stop', 'in'), ('stop', 'of'), ('noun', 'bear'), ('noun', 'princess')]

def scan(action):
    result = []
    words = action.split()
    for word in words:
        if word.isdigit():
            result.append(('number', int(word)))
        else:
            i, found = 0, False
            while (i < len(sentence)) and (found == False):
                if str.lower(word) == sentence[i][1]:
                    result.append(sentence[i])
                    found = True
                else:
                    i += 1
            if(found == False):
                result.append(('error', word))
    return result